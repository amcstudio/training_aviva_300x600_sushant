'use strict';

~(function() {
    var $ = gsap,
    bgExit = document.getElementById('bgExit'),
    ad = document.getElementById('mainContent'),
    disc = document.getElementById('disc'),
    closeBtn = document.getElementById('closeLegalsBut'),
    pulseCount = 0,
    pulseAnimation;

    window.init = function() {
        disc.addEventListener('click', openPannel);
        closeBtn.addEventListener('click', closePannel);
        bgExit.addEventListener('click', exitHandler);

        var tl = gsap.timeline({
        });
        tl.set('.vertBar', {scaleX:5})

        tl.addLabel('frameOne','+=2')
        tl.to('.vertBar', {x:0,scaleX:1, stagger: 0.2, ease: 'sine.inOut'},'frameOne')
        tl.to('#mainImgContainer', { duration: 5, scale:0.91,opacity: 1, ease: 'sine.inOut' }, 'frameOne')
        tl.to('#stripe', { duration: 3.8, height:290, opacity: 1, ease: 'sine.inOut' }, 'frameOne+=2.5')  
        tl.to('#headline', { duration:1.5, x:0,ease: 'sine.inOut' }, 'frameOne+=0.8')

        tl.to('#lineOne', { duration:0.5, scaleX:1, ease: 'sine.inOut' }, 'frameOne+=2.4')
        tl.to('#lineOne', { duration:0.5, opacity:0, ease: 'sine.inOut' }, 'frameOne+=2.8')
        tl.to('.copyOne', { duration:0.8, y:0, yoyo:true,repeat:1,repeatDelay:3, ease: 'sine.inOut' }, 'frameOne+=2.8')

        tl.addLabel('frameTwo','+=0.5')
        tl.to('#lineTwo', { duration:0.5, scaleX:1, ease: 'sine.inOut' }, 'frameTwo+=0.4')
        tl.to('.copyTwo', { duration:0.8, y:0, ease: 'sine.inOut' }, 'frameTwo+=0.8')
        tl.to('#cta', { duration: 0.34, opacity:1,  ease: 'sine.inOut' }, 'frameTwo+=1.1')
        tl.add(ctaPulse, 'frameTwo+=1.6')
}



function ctaPulse() {
    doPulse();
    pulseAnimation = setInterval(() => {
        if(pulseCount < 2) {
            doPulse();
            pulseCount++;
        }
        else {
            clearInterval(pulseAnimation)
        }
    }, 1500);
}

function doPulse() {
    var tlPulse = gsap.timeline();
    tlPulse.set('#cta', { boxShadow: "0px 0px 0px 0px rgba(255, 217, 0, 1)" }), 
    tlPulse.to('#cta', { duration:1.25, boxShadow: "0px 0px 8px 20px rgba(255, 217, 0, 0)", ease: 'power4.out' })
}


function openPannel() {
    var tlPannel = gsap.timeline();
    tlPannel.to('#legalsPanel', {  duration: .5, y:0, ease: 'power4.out'})
    console.log("Open")
}
function closePannel() {
    var tlPannel = gsap.timeline();
    tlPannel.to('#legalsPanel', {  duration: .5, y:600, ease: 'power4.out'})
    console.log("Close")
}

function exitHandler(e) {
    e.preventDefault();
    Enabler.exit('Background Exit');
}
})();